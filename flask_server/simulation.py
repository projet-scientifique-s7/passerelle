import random
import uuid

from flask import Flask, request

app = Flask(__name__)

demandes_simulation = []

"""
ATTENTION : Envoyer 0xFF envoie une demande d'interruption. NE PAS UTILISER 
"""


def gen_demandes():
    for _ in range(4):
        # On génère des demandes
        demandes_simulation.append(
            dict(capteur=random.randint(1, 255), intensite=random.randint(0, 10), req_code=uuid.uuid4())
        )


@app.route("/simulation/queue", methods=["GET"])
def queue_simulateur():
    if len(demandes_simulation) > 0:
        return demandes_simulation[0]
    gen_demandes()
    return "", 204


@app.route("/simulation/action", methods=["POST"])
def action_simulateur():
    if request.form.get("req_code"):
        demandes_simulation.pop(0)
        return {"status": "OK"}

    return {"error": True}, 400
