## Main program

from microbit import uart
import radio


key = "CHANGEME"

"""
length = Taille en byte d'un packet radio
channel = Fréquence d'écoute (de 0 à 83 inclut) 
power = puissance de 0 à 7 
address = Masque pour filtrer les packets
"""

radio.config(length=2, data_rate=radio.RATE_250KBIT, channel=47, address=0x75686979)
radio.on()
uart.init(115200)

print("capteur")

while True:
    buffer = bytearray(2)
    received = radio.receive_bytes_into(buffer)

    if received:
        uart.write(buffer)
