# Passerelle

Passerelle Inter MicroBit et Web


## Installation 

```shell
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Lancement

1. Connecter la microbit "capteur"
2. Récupérer son interface (`/dev/ttyACM0` généralement)
3. Connecter la microbit "transmetteur"
4. Récupérer son interface (`/dev/ttyACM1` généralement)
5. Lancer le serveur de simulation et applicatif (dans le dossier `flask_server`)
   * `python -m flask run simulation.py app -p 8001`
   * `python -m flask run application.py app -p 8002`
6. Lancer les passerelles (dossier `passerelles`)
   * `python transmetteur.py` (simulation)
   * `python capteur.py` (application)
   

## Résolution de problèmes

* Vérifier les interfaces dans les fichiers de config de la passerelle transmetteur et capteur
