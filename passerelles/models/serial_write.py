import multiprocessing as mp

import serial

from utils import log

class_logger = log.getChild("ecriture_serial")


class SerialWrite:
    def __init__(
        self,
        serial_queue: mp.Queue,
        mutex_serial: mp.Semaphore,
        ttydir: str,
        ttyspeed=11500,
    ):
        self.ttydir: str = ttydir
        self.ttyspeed: int = ttyspeed

        self.serial_queue: mp.Queue = serial_queue

        self.mutex_serial: mp.Semaphore = mutex_serial

        self.serial_conn = serial.Serial(self.ttydir, self.ttyspeed)

    def ecriture(self):
        while True:
            self.mutex_serial.acquire()

            class_logger.info("Attente données")
            message = self.serial_queue.get()
            class_logger.info("Message : %s" % bytes(message))
            self.serial_conn.flushOutput()

            self.serial_conn.write(bytes(message))
            class_logger.info("Message envoyé sur l'interface série")
