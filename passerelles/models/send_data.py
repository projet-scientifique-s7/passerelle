import json
import multiprocessing as mp
import paho.mqtt.client as mqtt
import uuid

import hashlib

import requests
from utils import log

class_logger = log.getChild("send_data")


class SendData:
    """
    Transmission des informations présentes sur la liaison série
    """

    def __init__(self, serial_queue: mp.Queue, mutex_serial: mp.Semaphore, no_passerelle: str,
                 code_passerelle: str, config_mqtt):
        self.serial_queue: mp.Queue = serial_queue

        self.no_passerelle = no_passerelle
        self.code_passerelle = code_passerelle

        self.mutex_serial: mp.Semaphore = mutex_serial

        self.client_mqtt = mqtt.Client("passerelle")
        #self.client_mqtt.on_connect = self.onconnect
        #self.client_mqtt.on_message = self.onmessage

        self.client_mqtt.username_pw_set(config_mqtt.get("USERNAME"), password=config_mqtt.get("PASSWORD"))

        self.client_mqtt.connect(config_mqtt.get("HOST"), port=int(config_mqtt.get("PORT")), keepalive=60)

    def onconnect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        self.client_mqtt.subscribe("$SYS/#")

    def onmessage(self, client, userdata, msg):
        print(msg.topic + " " + str(msg.payload))

    def build_request(self, code):
        salt = uuid.uuid4()

        print(code)

        request_signed = {
            "id_capteur": str(code[0]),
            "etat": code[1],
            "salt": str(salt),
            "id_passerelle": self.no_passerelle,
            "signature": hashlib.sha256(
                bytes(str(code[0]) + str(code[1]) + str(salt) + str(self.no_passerelle) + str(self.code_passerelle),
                      "UTF-8")).hexdigest()
        }

        return bytearray(json.dumps(request_signed), "UTF8")

    def main(self):
        while True:
            code = self.serial_queue.get()
            request_signed = self.build_request(code)

            response = self.client_mqtt.publish(f"sensors/{self.no_passerelle}/{code[0]}", payload=request_signed)
            if response.rc != mqtt.MQTT_ERR_SUCCESS:
                class_logger.info(response)
                continue
            class_logger.info("Requête publiée")
            class_logger.info(request_signed)
            class_logger.info("ID : %s" % response.mid)
