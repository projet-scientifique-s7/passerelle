import multiprocessing as mp

import serial

from utils import log

class_logger = log.getChild("lecture_serial")


class SerialRead:
    def __init__(
        self,
        serial_queue: mp.Queue,
        mutex_serial: mp.Semaphore,
        ttydir: str,
        ttyspeed=11500,
    ):
        self.ttydir: str = ttydir
        self.ttyspeed: int = ttyspeed

        self.serial_queue: mp.Queue = serial_queue

        self.mutex_serial: mp.Semaphore = mutex_serial

        self.serial_conn = serial.Serial(self.ttydir, self.ttyspeed)

    def lecture(self):
        class_logger.info("Ecoute sur l'interface série")
        while True:
            data = self.serial_conn.read(2)
            print(data)
            if data:
                class_logger.info("Read")
                class_logger.warn("s: \n%s" % bytes(data))
                print(len(data))

                if len(data) == 2:
                    id_sensor = data[0]
                    intensite = data[1]
                    class_logger.info("id_sensor: %s" % (id_sensor))
                    class_logger.info("intensity: %s" % (intensite))
                    self.serial_queue.put(data)
                else:
                    class_logger.warn("Data not valid : \n%s" % data)
