import hashlib
import json
import multiprocessing as mp

import requests
import time
from utils import log

class_logger = log.getChild("pull_data")


class PullData:
    """
    Récupération périodique des données du simulateur avant de transmettre au transmetteur via la liaison série
    """

    def __init__(self, serial_queue: mp.Queue, mutex_serial: mp.Semaphore, server_url: str):
        self.serial_queue: mp.Queue = serial_queue

        self.server_url = server_url

        self.mutex_serial: mp.Semaphore = mutex_serial

        self.previous_data_sha = None

    def get_code(self, capteur, intensite):
        code = bytearray(2)
        code[0] = capteur
        code[1] = intensite
        return code

    def main(self):
        while True:
            response = requests.get(self.server_url + "/sensor/active/", verify="./models/swano_ca_srv.crt")
            if response.status_code != 200:
                class_logger.info(response.text)
                continue

            data = response.json()
            data_sha = hashlib.sha512(bytes(json.dumps(data), "UTF8")).hexdigest()

            if self.previous_data_sha != data_sha or True: # On renvoie uniquement si il y a eu un changement sur les données transmises
                class_logger.info("Update SHA " + data_sha)
                class_logger.info(response.json())

                self.previous_data_sha = data_sha
                for capteur in data:
                    code = self.get_code(capteur.get("idApp"), capteur.get("etat"))
                    self.serial_queue.put(code)
                    self.mutex_serial.release()

            time.sleep(1)
