import multiprocessing as mp

import configparser

from models import SerialWrite, PullData
from utils import log

main_logger = log.getChild("main_logger")


if __name__ == "__main__":
    main_logger.info("Initialisation des sémaphores et de la Queue")
    serial_queue = mp.Queue(maxsize=1)

    mutex_serial = mp.Semaphore(0)
    main_logger.info("Lecture de la configuration")
    configuration = configparser.ConfigParser()
    configuration.read("config_transmetteur.cfg")
    main_logger.info("Initialisation de la lecture série")

    serial_write = SerialWrite(
        serial_queue=serial_queue,
        mutex_serial=mutex_serial,
        ttydir=configuration["SERIAL"]["DIR"],
        ttyspeed=configuration["SERIAL"]["SPEED"],
    )

    pull_data = PullData(
        serial_queue=serial_queue, mutex_serial=mutex_serial, server_url=configuration["SERVER"]["URL"]
    )

    main_logger.info("Initialisation terminée")

    thread_serial_write = mp.Process(target=serial_write.ecriture)
    thread_pull_data = mp.Process(target=pull_data.main)

    thread_pull_data.start()
    thread_serial_write.start()

    thread_serial_write.join()
    thread_pull_data.join()
