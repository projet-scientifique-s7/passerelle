import logging

log = logging.getLogger("passerelle_iot")
log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)
