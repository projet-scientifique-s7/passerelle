import multiprocessing as mp

import configparser

from models import SendData, SerialRead
from utils import log

main_logger = log.getChild("main_logger")

if __name__ == "__main__":
    main_logger.info("Initialisation des sémaphores et de la Queue")
    serial_queue = mp.Queue(maxsize=1)

    mutex_serial = mp.Semaphore(0)
    main_logger.info("Lecture de la configuration")
    configuration = configparser.ConfigParser()
    configuration.read("config_capteur.cfg")
    main_logger.info("Initialisation de la lecture série")

    serial_read = SerialRead(
        serial_queue=serial_queue,
        mutex_serial=mutex_serial,
        ttydir=configuration["SERIAL"]["DIR"],
        ttyspeed=configuration["SERIAL"]["SPEED"],
    )

    send_data = SendData(
        serial_queue=serial_queue,
        mutex_serial=mutex_serial,
        no_passerelle=configuration["SERVER"]["NO_PASSERELLE"],
        code_passerelle=configuration["SERVER"]["CODE_PASSERELLE"],
        config_mqtt=configuration["MQTT"]
    )

    main_logger.info("Initialisation terminée")

    thread_serial_read = mp.Process(target=serial_read.lecture)
    thread_send_data = mp.Process(target=send_data.main)

    thread_send_data.start()
    thread_serial_read.start()

    thread_serial_read.join()
    thread_send_data.join()
